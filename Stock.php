<?php

namespace Petshop\Service;
use Petshop\Service\ServiceAbstract;

class Stock extends ServiceAbstract {
    const DEFAULT_QUANTITY = 0,
        STATUS_EXIST_IN_WAREHOUSE = 2, // есть на складе
        STATUS_AWAITING_IN_SUPPLY = 4, // ожидается в поставке
        STATUS_EXIST_IN_SUPPLIER = 6, // есть у поставщика
        STATUS_NOT_AVAILABLE = 8; // нигде нет

    protected static $sCacheKey = 'stock:';
    public static $aCache = [], $sTableName = null;

    public static $aStatusNames = [
        self::STATUS_EXIST_IN_WAREHOUSE => 'в наличии',
        self::STATUS_AWAITING_IN_SUPPLY => 'в наличии', // в пути
        self::STATUS_EXIST_IN_SUPPLIER => 'под заказ',
        self::STATUS_NOT_AVAILABLE => 'нет в наличии',
    ];


    /**
     * @param $aProductIds
     * @param $iRegionId
     * @param array $aProductsQuantity
     * @param string $sSiteId
     * @param \Petshop\Model\Delivery\DeliveryAbstract $oDelivery
     * @return array
     * @throws \Exception
     */
    public static function getProductsStatus($aProductIds, $iRegionId, $aProductsQuantity = [], $sSiteId = SITE_ID, $oDelivery = null) {
        $aProductsStock = [];
        foreach ($aProductIds as $iProductId) {
            $aProductsStock[$iProductId] = [
                'status' => null,
                'stock_days' => null,
                'shipment_days' => null,
                'delivery_days' => null,
                'total_days' => null,
                'delivery_service' => $oDelivery ? $oDelivery->getDeliveryId() : null,
            ];
        }

        // Применить срок поставки до склада
        self::applySupplyDays($aProductsStock, $iRegionId, $aProductsQuantity, $sSiteId);

        if ($oDelivery) {
            // Применить срок отгрузки службе доставки
            self::applyShipmentDays($aProductsStock, $sSiteId, $oDelivery);

            // Применить срок доставки
            self::applyDeliveryDays($aProductsStock, $oDelivery);
        }

        return $aProductsStock;
    }

    /**
     * Рассчитать срок поставки товара от поставщика на склад
     * @param $aProductsStock
     * @param $iRegionId
     * @param $aProductsQuantity
     * @throws \Exception
     * @return bool
     */
    protected static function applySupplyDays(&$aProductsStock, $iRegionId, $aProductsQuantity, $sSiteId) {
        $aWarehouseIds = \Petshop\Service\Warehouse::findByParams(null, $iRegionId, true);

        if (!$aWarehouseIds) {
            throw new \Exception('No primary warehouse for region ' . $iRegionId);
        }

        // проверить остатки на складах
        self::checkRemainsInWarehouses($aProductsStock, $aWarehouseIds, $aProductsQuantity, $sSiteId);

        // проверить наличие в ближайших поставках
        self::checkAtNearestSupply($aProductsStock, $aWarehouseIds);

        // проверить в наличии у поставщиков
        self::checkRemainsAtSuppliers($aProductsStock, $iRegionId, $sSiteId);

        self::checkNotAvailable($aProductsStock, $sSiteId);

        return true;
    }

    /**
     * Проверить остатки на складах
     * @param $aProductsStock
     * @param $aWarehouseIds
     * @param $aProductsQuantity
     * @param $sSiteId
     * @return bool
     */
    protected static function checkRemainsInWarehouses(&$aProductsStock, $aWarehouseIds, $aProductsQuantity, $sSiteId) {
        $aProductIds = array_keys($aProductsStock);
        $aStocks = \Petshop\Service\Stock::getProductsQuantity($aProductIds);

        foreach ($aProductIds as $iProductId) {
            $aProductStock = $aStocks[$iProductId];

            /** @var  $iDesiredQuantity - необходимое кол-во товаров */
            $iDesiredQuantity = isset($aProductsQuantity[$iProductId]) ? $aProductsQuantity[$iProductId] : 0;

            $iRemainsQuantity = 0;

            foreach ($aWarehouseIds as $iWarehouseId) {
                if (!isset($aProductStock[$iWarehouseId]) || ($aProductStock[$iWarehouseId]['quantity'] <= 0)) {
                    continue;
                }
                $iRemainsQuantity += $aProductStock[$iWarehouseId]['quantity'];
            }

            if ($iRemainsQuantity && $iDesiredQuantity <= $iRemainsQuantity) {
                $aProductsStock[$iProductId]['status'] = self::STATUS_EXIST_IN_WAREHOUSE;
                $aProductsStock[$iProductId]['stock_days'] = [0];
                $aProductsStock[$iProductId]['total_days'] = $aProductsStock[$iProductId]['stock_days'];
            } elseif (in_array($sSiteId, \Petshop\Config\Delivery::$aIgnoreSupplies)) {
                // Для некоторых сайтов отсутствие товаров означает доставку в 3-7 дней
                $aProductsStock[$iProductId]['status'] = self::STATUS_NOT_AVAILABLE;
                $aProductsStock[$iProductId]['stock_days'] = \Petshop\Config\Delivery::$aDeliveryDaysWhenNotAvailable[$sSiteId];
                $aProductsStock[$iProductId]['total_days'] = $aProductsStock[$iProductId]['stock_days'];
            }
        }
        return true;
    }

    /**
     * Поиск товара в ближайщей поставке
     * @param $aProductsStock
     * @param $aWarehouseIds
     * @return bool
     */
    protected static function checkAtNearestSupply(&$aProductsStock, $aWarehouseIds) {
        $aProductIds = [];
        foreach ($aProductsStock as $iProductId => $aProductStock) {
            if (!is_null($aProductStock['status'])) {
                continue;
            }
            $aProductIds[] = $iProductId;
        }

        if (!$aProductIds) {
            return false;
        }

        // поиск поставок на склады
        $cSupplies = \Petshop\Assistance\Supply::findByWarehouseIds($aWarehouseIds, time());
        if (!$cSupplies->count()) {
            return false;
        }

        // поиск товаров в найденных поставках
        $aProductsSupply = \Petshop\Service\Supply::findProductsBySupplyIds($cSupplies->getIds());

        $iMinSupplyDays = null;
        foreach ($aProductIds as $iProductId) {
            foreach ($cSupplies as $oSupply) {
                /** @var \Petshop\Model\Supply $oSupply */
                if (!isset($aProductsSupply[$oSupply->getId()]) || !in_array($iProductId, $aProductsSupply[$oSupply->getId()])) {
                    continue;
                }

                $iSupplyDays = $oSupply->getDays();

                if (!isset($iMinSupplyDays) || $iMinSupplyDays > $iSupplyDays) {
                    $iMinSupplyDays = $iSupplyDays;

                    $aProductsStock[$iProductId]['status'] = self::STATUS_AWAITING_IN_SUPPLY;
                    $aProductsStock[$iProductId]['stock_days'] = [$iSupplyDays];
                    $aProductsStock[$iProductId]['total_days'] = [$iSupplyDays];
                }
            }
        }

        return true;
    }

    /**
     * Проверить остатки у поставщиков
     * @param $aProductsStocks
     * @param $iRegionId
     * @return bool
     */
    protected static function checkRemainsAtSuppliers(&$aProductsStocks, $iRegionId, $sSiteId) {
        $aProductIds = array_keys($aProductsStocks);
        $aProductSuppliers = \Petshop\Service\StockSupplier::getSupplierIdsByProductIds($aProductIds);
        if (!$aProductSuppliers) {
            return false;
        }

        $aSupplierIds = array_unique(array_values($aProductSuppliers));

        $aDeliveryDays = \Petshop\Service\SupplierDelivery::getDeliveryDays($aSupplierIds, $iRegionId);

        $iTimestampStart = time();
        foreach ($aProductsStocks as $iProductId => $aProductStock) {
            $iSupplierId = isset($aProductSuppliers[$iProductId]) ? $aProductSuppliers[$iProductId] : null;

            if (!is_null($aProductStock['status'])) {
                continue;
            } elseif (!$iSupplierId) {
                continue;
            } elseif (!isset($aDeliveryDays[$iSupplierId]) || !$aDeliveryDays[$iSupplierId]) {
                continue;
            }

            $aDays = $aDeliveryDays[$iSupplierId];

            if ($aProductStock['status'] == self::STATUS_AWAITING_IN_SUPPLY && (isset($aProductStock['stock_days']) && $aProductStock['stock_days'][0] <= $aDays[0])) {
                // ожидается в более ранней поставке
                continue;
            }

            // Срок с учетом выходных
            foreach ($aDays as $iKey => $iDay) {
                $aDays[$iKey] = \Petshop\Helper\Delivery::calculateWorkDaysPeriod(false, $iTimestampStart, $iDay, $sSiteId);
            }

            $aProductsStocks[$iProductId]['status'] = self::STATUS_EXIST_IN_SUPPLIER;
            $aProductsStocks[$iProductId]['stock_days'] = $aDays;
            $aProductsStocks[$iProductId]['total_days'] = $aDays;
        }

        return true;
    }

    /**
     * Проставляет STATUS_NOT_AVAILABLE тем товарам, у которых статус не был выставлен на более ранних этапах
     * @param type $aProductsStock
     */
    protected static function checkNotAvailable(&$aProductsStock, $sSiteId) {
        foreach ($aProductsStock as &$aStock) {
            if (is_null($aStock['status'])) {
                if (isset(\Petshop\Config\Delivery::$aDeliveryDaysWhenNotAvailable[$sSiteId])) {
                    $aStock['stock_days'] = \Petshop\Config\Delivery::$aDeliveryDaysWhenNotAvailable[$sSiteId];
                    $aStock['total_days'] = \Petshop\Config\Delivery::$aDeliveryDaysWhenNotAvailable[$sSiteId];
                }
                $aStock['status'] = self::STATUS_NOT_AVAILABLE;
            }
        }
    }

    /**
     *  Срок отгрузки товаров со склада кур. службе
     * @param $aProductsStock
     * @param $sSiteId
     * @param \Petshop\Model\Delivery\DeliveryAbstract $oDelivery
     */
    protected static function applyShipmentDays(&$aProductsStock, $sSiteId, \Petshop\Model\Delivery\DeliveryAbstract $oDelivery) {
        foreach ($aProductsStock as $iProductId => $aStock) {
            if ($aStock['status'] == self::STATUS_NOT_AVAILABLE) {
                continue;
            }

            // срок отгрузки считается от срока поставки
            $aShipmentDaysTotal = [];
            foreach ($aProductsStock[$iProductId]['stock_days'] as $iDays) {
                $iStockTimestamp = time() + $iDays * 86400;
                $aShipmentDays = $oDelivery->getShipmentDays($sSiteId, $iStockTimestamp);
                sort($aShipmentDays);

                $iMaxDay = array_pop($aShipmentDays) ? : 0;
                $iMinDay = array_shift($aShipmentDays) ? : 0;

                if (!isset($aShipmentDaysTotal[0]) || $aShipmentDaysTotal[0] > $iMinDay) {
                    $aShipmentDaysTotal[0] = $iMinDay;
                }

                if (!isset($aShipmentDaysTotal[1]) || $iMaxDay > $aShipmentDaysTotal[1]) {
                    $aShipmentDaysTotal[1] = $iMaxDay;
                }
            }

            $aProductsStock[$iProductId]['shipment_days'] = $aShipmentDaysTotal;
            $aProductsStock[$iProductId]['total_days'] = self::sumUpTotalDays($aProductsStock[$iProductId]['total_days'], $aShipmentDaysTotal);
        }
        unset($aProductsStock);
    }

    /**
     * Срок доставки службой со склада к клиенту
     * @param $aProductsStock
     * @param \Petshop\Model\Delivery\DeliveryAbstract $oDelivery
     */
    protected static function applyDeliveryDays(&$aProductsStock, \Petshop\Model\Delivery\DeliveryAbstract $oDelivery) {
        $aDeliveryDays = $oDelivery->getDays();
        if (!is_array($aDeliveryDays)) {
            $aDeliveryDays = [$aDeliveryDays];
        }

        foreach ($aProductsStock as $iProductId => $aStock) {
            if ($aStock['status'] == self::STATUS_NOT_AVAILABLE) {
                continue;
            }
            $aProductsStock[$iProductId]['delivery_days'] = $aDeliveryDays;
            $aProductsStock[$iProductId]['total_days'] = self::sumUpTotalDays($aProductsStock[$iProductId]['total_days'], $aDeliveryDays);
        }

        unset($aProductsStock);
    }

    /**
     * @param $aCurrentDays
     * @param $aDaysForSum
     * @return mixed
     */
    protected static function sumUpTotalDays($aCurrentDays, $aDaysForSum) {
        $aTotal = [];
        if (($iNumCurrent = count($aCurrentDays)) != ($iNumForSumm = count($aDaysForSum))) {
            if ($iNumCurrent < $iNumForSumm) {
                $aCurrentDays = $aDaysForSum;
                $aDaysForSum = func_get_arg(0);
            }
        }

        foreach ($aCurrentDays as $iKey => $iDays) {
            $iKeySumm = isset($aDaysForSum[$iKey]) ? $iKey : $iKey - 1;
            $aTotal[$iKey] = $iDays + $aDaysForSum[$iKeySumm];
        }

        return $aTotal;
    }


    /**
     * @param int $iProductId
     * @param int $iWarehouseId
     * @param int $iQuantity
     * @param string $sDeliveryDate ex. 2014-03-12
     */
    public static function setProductQuantity($iProductId, $iWarehouseId, $iQuantity, $sDeliveryDate = null) {
        self::setProductsQuantity([
            $iProductId => [
                $iWarehouseId => [$iQuantity, $sDeliveryDate],
            ],
        ]);

        \Petshop\Assistance\Search\SearchTask::addUpdateProductsStock([$iProductId]);
    }

    /**
     * Установка количества товаров на складах
     * Формат $aData = [
     *     <product_id1> => [
     *         <warehouse1_id> => [<quantity>, <delivery_date>],
     *         <warehouse2_id> => [<quantity>, null],
     *         ...
     *     ],
     *     ...
     * ]
     *
     * @param array $aData
     * @param bool $bFlushStock - сбрасывать ли имеющиеся в БД остатки
     */
    public static function setProductsQuantity($aData, $bFlushStock = false) {
        if (!$aData) {
            return false;
        }

        $c = self::getConnection();
        $aProductIds = array_keys($aData);

        $aBind = [];
        $aValues = [];
        foreach ($aData as $iProductId => $aStock) {
            foreach ($aStock as $iWarehouseId => $aWarehouseStock) {
                $aBind[] = $iProductId;
                $aBind[] = $iWarehouseId;
                $aBind[] = $aWarehouseStock[0];
                $aValues[] = '(?,?,?)';
            }
        }

        // Flush current values
        if ($bFlushStock) {
            $stmt = $c->prepare('DELETE FROM api_stock WHERE product_id IN (' . implode(',', array_fill(0, count($aProductIds), '?')) . ')');
            $stmt->execute($aProductIds);
        }

        // Insert values
        if ($aValues) {
            $stmt = $c->prepare('
                INSERT INTO api_stock (product_id, warehouse_id, quantity)
                VALUES ' . implode(', ', $aValues) . '
                ON DUPLICATE KEY UPDATE product_id = VALUES(product_id), warehouse_id = VALUES(warehouse_id), quantity = VALUES(quantity)
            ');
            $stmt->execute($aBind);
        }

        // Clear cache
        self::flushCacheByIds($aProductIds);

        \Petshop\Assistance\Search\SearchTask::addUpdateProductsStock($aProductIds);
    }

    /**
     * Возвращает массив с остатками товаров и ожидаемой датой поставки.
     * Сгруппирован по ID товара, потом по ID склада.
     *
     * @param array $aProductIds
     * @param bool $bFlushCache
     * @return array
     */
    public static function getProductsQuantity($aProductIds, $bFlushCache = false) {
        if (!$aProductIds) {
            return [];
        } elseif (!is_array($aProductIds)) {
            $aProductIds = [$aProductIds];
        }

        $c = self::getConnection();
        $MC = \Petshop\Db\Db::getCache();

        $aResult = [];
        $aKeys = self::getCacheKeys($aProductIds);

        // Try to get from cache
        if (!$bFlushCache) {
            $aCachedData = $MC->get($aKeys);
            foreach ($aCachedData as $sKey => $aValue) {
                $sKey = substr($sKey, 6);
                $aResult[$sKey] = $aValue;
            }

            $aNotCachedIds = array_diff($aProductIds, array_keys($aResult));
        } else {
            $aNotCachedIds = $aProductIds;
        }

        // Try to get from DB
        if ($aNotCachedIds) {
            $stmt = $c->prepare('SELECT * FROM api_stock WHERE product_id IN (' . implode(',', array_fill(0, count($aProductIds), '?')) . ')');
            $stmt->execute($aProductIds);

            while ($aStock = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $aResult[$aStock['product_id']][$aStock['warehouse_id']] = [
                    'quantity' => $aStock['quantity'],
                ];
            }

            foreach ($aNotCachedIds as $iProductId) {
                if (!isset($aResult[$iProductId])) {
                    $aResult[$iProductId] = [];
                }

                $sKey = $aKeys[$iProductId];
                $MC->set($sKey, $aResult[$iProductId]);
            }
        }

        return $aResult;
    }

    /**
     * @param $iStatusId
     * @param null $sSiteId
     * @return string
     */
    public static function getStatusName($iStatusId, $sSiteId = null) {
        if (!isset(self::$aStatusNames[$iStatusId])) {
            return '';
        }

        if ($iStatusId == self::STATUS_NOT_AVAILABLE) {
            if (isset(\Petshop\Config\Delivery::$aDeliveryDaysWhenNotAvailable[$sSiteId])) {
                return '';
            }
        } elseif ($iStatusId == self::STATUS_EXIST_IN_SUPPLIER) {
            if (in_array($sSiteId, [\Petshop\Config\Petshop::SITE_KD])) {
                return '';
            }
        }

        return self::$aStatusNames[$iStatusId];
    }
}
